import { Component, OnInit } from '@angular/core';
import { ICustomer } from '../shared/interfaces';
import { CustomersService } from '../core/services/customers.service';

@Component({
  selector: 'bs-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  title: string;
  customers: ICustomer[] = [];
  displayMode: DisplayModeEnum;
  displayModeEnum = DisplayModeEnum;
  totalRecords = 0;
  pageSize = 10;

  constructor(private customerService: CustomersService) { }

  ngOnInit(): void {
    this.title = 'Customers';
    this.displayMode = DisplayModeEnum.Card;
    this.getCustomers();
  }

  changeDisplayMode(mode: DisplayModeEnum) {
    this.displayMode = mode;
  }

  getCustomers() {
    this.customerService.getCustomers()
        .subscribe((response: ICustomer[]) => {
          this.customers = response;
        },
        (err: any) => console.log(err),
        () => console.log('getCustomersPage() retrieved all the customers'));
  }
}

enum DisplayModeEnum {
  Card = 0,
  Grid = 1,
}