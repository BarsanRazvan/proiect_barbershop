import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersComponent } from './customers.component';
import { UserCanActivateGuard } from '../core/guards/user-can-activate.guard';

const routes: Routes = [
  {path : '', component: CustomersComponent, canActivate: [UserCanActivateGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule {
  static components = [CustomersComponent];
 }
