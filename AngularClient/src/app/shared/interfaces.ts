import { ModuleWithProviders } from '@angular/core';
import { Token } from '@angular/compiler/src/ml_parser/lexer';

export interface IUserLogin {
    username: string;
    password: string;
}

export interface ICurrentUser {
    id: number;
    name: string;
    role: string;
    token?: Token;
}

export interface ICustomer {
    customerId: number;
    firstName: string;
    lastName: string;
    facebookProfile: string;
    phoneNumber: string;
    nrOfAppointments: number;
}

export interface IPagedResults<T> {
    totalRecords: number;
    results: T;
}
