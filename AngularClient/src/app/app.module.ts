import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginModule } from './login/login.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HomeModule } from './home/home.module';

@NgModule({
  imports: [
    BrowserModule,
    LoginModule,
    HomeModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    NoopAnimationsModule  
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
