import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../services/authentication.service';
import { ICurrentUser } from 'src/app/shared/interfaces';
import { Subscription } from 'rxjs';

@Component({
  selector: 'bs-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public navbarOpen = false;
  public loginLogoutText = 'Login';
  sub: Subscription;

  constructor(private userService: AuthenticationService, private router: Router){
  }
  
  getCurrentUser(){
    return this.userService.currentUserValue;
  }

  ngOnInit(): void {
    this.setLoginLogoutText();
    this.sub = this.userService.authChanged
    .subscribe((loggedIn: boolean) => {
        this.setLoginLogoutText();
    });
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  setLoginLogoutText() {
    this.loginLogoutText = (this.userService.currentUserValue) ? 'Logout' : 'Login';
  }

  redirectToLogin() {
    this.router.navigate(['/login']);
  }

  loginOrOut() {
    if(this.userService.currentUserValue){
      this.userService.logout();
      this.setLoginLogoutText();
      return;
    }
    this.redirectToLogin();
  }
}
