import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { NavbarComponent } from './navbar/navbar.component';
import { EnsureModuleLoadedOnceGuard } from './ensure-module-loaded-once.guard';

import { AuthenticationService } from './services/authentication.service';
import { AuthenticationInterceptor } from './interceptors/authentication.interceptor';
import { CustomersService } from './services/customers.service';
import { AdminCanActivateGuard } from './guards/admin-can-activate.guard';
import { UserCanActivateGuard } from './guards/user-can-activate.guard';

@NgModule({
  imports: [CommonModule, HttpClientModule, RouterModule],
  exports: [RouterModule, HttpClientModule, NavbarComponent], 
  declarations: [NavbarComponent],
  providers: [AuthenticationService, CustomersService, AdminCanActivateGuard, UserCanActivateGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthenticationInterceptor,
    multi: true,
  }]
})
export class CoreModule extends EnsureModuleLoadedOnceGuard{
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    super(parentModule);
  }
 }
