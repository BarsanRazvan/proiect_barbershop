import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IUserLogin, ICurrentUser } from 'src/app/shared/interfaces';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class AuthenticationService {
  private authUrl = `${environment.apiUrl}/accounts/authenticate/`;
  private currentUserSubject: BehaviorSubject<ICurrentUser>;
  public currentUser: Observable<ICurrentUser>;
  @Output() authChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private http: HttpClient) { 
    this.currentUserSubject = new BehaviorSubject<ICurrentUser>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): ICurrentUser {
    return this.currentUserSubject.value;
 }

  login(userLogin: IUserLogin){
    console.log(this.authUrl);
    return this.http.post<any>(this.authUrl, userLogin)
        .pipe(
            map(user => {
                console.log("trying");
                if(user && user.token){
                  localStorage.setItem('currentUser', JSON.stringify(user));
                  this.currentUserSubject.next(user);
                  this.userAuthChanged(true);
                }
                return user;
            }),
            catchError(this.handleError)
        );
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.userAuthChanged(false);
    this.currentUserSubject.next(null);
  }

  private handleError(error: HttpErrorResponse) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }

  private userAuthChanged(status: boolean) {
    this.authChanged.emit(status); // Raise changed event
  }
}
