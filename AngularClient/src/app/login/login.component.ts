import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../core/services/authentication.service';
import { IUserLogin } from '../shared/interfaces';
import { first } from 'rxjs/operators';

@Component({
  selector: 'bs-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit{
  loginForm: FormGroup;
  errorMessage: string;
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.formBuilder.group({
        username:      ['', [ Validators.required]],
        password:   ['', [ Validators.required]]
    });
  }

  submit({ value, valid }: { value: IUserLogin, valid: boolean }) {
    this.authService.login(value)
    .pipe(first())
    .subscribe(
        data => {
            this.router.navigate(['/home']);
        },
        error => {
            this.errorMessage = "Invalid credentials";
        });
  }
}
