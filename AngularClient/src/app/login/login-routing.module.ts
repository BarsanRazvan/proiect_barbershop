import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';
import { CanActivateGuard } from './guards/can-activate.guard';

const routes: Routes = [  
  { path: 'login', component: LoginComponent,  canActivate: [CanActivateGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanActivateGuard]
})
export class LoginRoutingModule {
  static components = [ LoginComponent ];
 }
