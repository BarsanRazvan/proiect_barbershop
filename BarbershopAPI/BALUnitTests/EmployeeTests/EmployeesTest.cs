﻿using AutoMapper;
using DAL.Interfaces;
using DAL.Entities;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using BAL.Models;
using System.Linq;
using System.Linq.Expressions;
using BAL.Services;

namespace BALUnitTests.EmployeeTests
{
    
    public class EmployeesTest
    {
        [Fact]
        public void testUpdate()
        {
            Employees oldEmployes = new Employees
            {
                EmployeeId = 1,
                Role = new UserRoles
                {
                    RoleId = 1,
                    RoleName = "ADMIN"
                },
                PasswordHash = "barsanparolatare"
            };

            EmployeeModel newEmployee = new EmployeeModel
            {
                Username = "barsan",
                FirstName = "Barsan",
                RoleName = "ADMIN",
                LastName = "Barsan",
                NoOfAppointments = 0,
                Password = null
            };

            Employees mappedEmployee = new Employees
            {
                Username = "barsan",
                FirstName = "Barsan",
                Role = null,
                LastName = "Barsan",
                NoOfAppointments = 0,
                PasswordHash = null
            };

            IEnumerable<UserRoles> roles = new List<UserRoles>();
            roles.Append(oldEmployes.Role);

            var mockUFW = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            mockUFW.Setup(p => p.Employees.GetByID(1)).Returns(oldEmployes);
            mockUFW.Setup(p => p.Roles.Get(It.IsAny<Expression<Func<UserRoles, bool>>>(), null, null)).Returns(roles);
            mockMapper.Setup(p=> p.Map<Employees>(newEmployee)).Returns(mappedEmployee);

            EmployeeService service = new EmployeeService(mockUFW.Object, mockMapper.Object);
            service.UpdateEmployee(newEmployee);
        }
    }
}
