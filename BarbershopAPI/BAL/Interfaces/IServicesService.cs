﻿using BAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Interfaces
{
    public interface IServicesService
    {
        int AddNewService(ServiceModel service);
        int RemoveService(int id);
        int UpdateService(ServiceModel service);
        IEnumerable<ServiceModel> GetServices();
        ServiceModel GetService(int serviceId);
    }
}
