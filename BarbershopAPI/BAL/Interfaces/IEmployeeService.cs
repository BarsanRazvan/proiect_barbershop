﻿using BAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Interfaces
{
    public interface IEmployeeService
    {
        EmployeeModel CheckCredentials(String username, String password);
        int AddNewEmployee(EmployeeModel employee);
        IEnumerable<EmployeeModel> GetEmployees();
        int DeleteEmployee(int employeeId);
        int AddUnavailableDay(int employeeId, DateTime day);
        int RemoveUnavailableDay(int employeeId, DateTime day);
        EmployeeModel GetEmployee(int employeeId);
        IEnumerable<UnavailableDayModel> GetUnavailableDays(int employeeId);
    }
}
