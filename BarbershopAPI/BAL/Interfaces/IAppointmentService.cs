﻿using BAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Interfaces
{
    public interface IAppointmentService
    {
        int MakeNewAppointment(AppointmentModel appointment);
        int FinishAppointment(int appointmentId);
        AppointmentModel GetAppointmentByDateTime(DateTime date, int employeeID );
        IEnumerable<AppointmentModel> GetAppointmentsByDate(DateTime date, int employeeId, bool finished);
        int CancelAppointment(int appointmentId);
    }
}
