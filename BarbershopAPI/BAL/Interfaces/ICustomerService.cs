﻿using BAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Interfaces
{
    public interface ICustomerService
    {
        int AddNewCustomer(CustomerModel customer);
        PagedModel<CustomerModel> GetCustomers(int pageNumber, int pageSize);
        int UpdateCustomer(CustomerModel customer);
        int AddBlackList(int customerId, string reason);
        int RemoveBlackList(int customerId);
        CustomerModel GetCustomer(string phoneNumber);
        CustomerModel GetCustomer(int customerId);

        public BlackListModel CheckBlacklist(int customerId);
    }
}
