﻿using AutoMapper;
using BAL.Interfaces;
using BAL.Models;
using BAL.Utilities;
using DAL.Entities;
using DAL.Interfaces;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAL.Services
{
    public class AppointmentService : IAppointmentService
    {
        private IUnitOfWork unitOfWork;
        private IMapper _mapper;

        public AppointmentService()
        {
            unitOfWork = new UnitOfWork();
            _mapper = CustomAutoMapper.GetInstance().Configuration.CreateMapper();
        }

        public int FinishAppointment(int appointmentId)
        {
            Appointments appointment = unitOfWork.Appointments.GetByID(appointmentId);
            appointment.StateOfAppointment = 1;
            unitOfWork.Appointments.Update(appointment);
            unitOfWork.Save();
            return 0;
        }

        public AppointmentModel GetAppointmentByDateTime(DateTime date, int employeeId)
        {
            //TO DO: EXCEPTION HANDLING

            Appointments appointment = unitOfWork.Appointments.Get(appointment => DateTime.Compare(date, appointment.DateOfAppointment) == 0
                                                                                   && appointment.EmployeeId == employeeId
                                                                                   && appointment.StateOfAppointment != 2) 
                                                               .FirstOrDefault(); ;
            
            if (appointment != null)
            {
                return _mapper.Map<AppointmentModel>(appointment);
            }

            return null;
        }

        public IEnumerable<AppointmentModel> GetAppointmentsByDate(DateTime date, int employeeId, bool finished)
        {
            //TO DO: EXCEPTION HANDLING
            int state = finished ? 1 : 0;

            return unitOfWork.Appointments
                    .Get(appointment => DateTime.Compare(date.Date, appointment.DateOfAppointment.Date) == 0
                                        && appointment.EmployeeId == employeeId
                                        && appointment.StateOfAppointment == state)
                    .Select(t => _mapper.Map<AppointmentModel>(t));
        }

        public int MakeNewAppointment(AppointmentModel appointment)
        {
            Appointments existingAppointment = unitOfWork.Appointments.Get(app => DateTime.Compare(app.DateOfAppointment, appointment.DateOfAppointment) == 0
                                                                                 && app.EmployeeId == appointment.EmployeeId
                                                                                 && app.StateOfAppointment != 2).FirstOrDefault();
            if (existingAppointment != null)
            {
                return -1;
            }

            unitOfWork.Appointments.Insert(_mapper.Map<Appointments>(appointment));
            unitOfWork.Save();
            return 0;
        }

        public int CancelAppointment(int appointmentId) 
        {
            //TO DO: EXCEPTION HANDLING
            Appointments appointment = unitOfWork.Appointments.GetByID(appointmentId);
            if (appointment == null)
            {
                return -1;
            }
            appointment.StateOfAppointment = 2;
            unitOfWork.Appointments.Update(appointment);
            unitOfWork.Save();

            return 1;
        }
    }
}
