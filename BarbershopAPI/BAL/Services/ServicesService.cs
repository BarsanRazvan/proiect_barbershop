﻿using AutoMapper;
using BAL.Interfaces;
using BAL.Models;
using BAL.Utilities;
using DAL.Entities;
using DAL.Interfaces;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAL.Services
{
    public class ServicesService : IServicesService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper _mapper;

        public ServicesService()
        {
            unitOfWork = new UnitOfWork();
            _mapper = CustomAutoMapper.GetInstance().Configuration.CreateMapper();
        }

        public int AddNewService(ServiceModel service)
        {
            //validations + error handling
            unitOfWork.Services.Insert(_mapper.Map<OfferedServices>(service));
            unitOfWork.Save();
            return 0;
        }

        public IEnumerable<ServiceModel> GetServices()
        {
            IEnumerable<ServiceModel> services = unitOfWork.Services.Get(serv => serv.Active == true)
                                                  .Select(serv => _mapper.Map<ServiceModel>(serv));
            return services;
        }

        public ServiceModel GetService(int serviceId)
        {
            OfferedServices service = unitOfWork.Services.GetByID(serviceId);
            if(service == null)
            {
                return null;
            }

            return _mapper.Map<ServiceModel>(service);
        }


        public int RemoveService(int id)
        {
            OfferedServices service = unitOfWork.Services.GetByID(id);

            if(service == null)
            {
                return -1;
            }

            service.Active = false;
            unitOfWork.Services.Update(service);
            unitOfWork.Save();

            return 0;
        }

        public int UpdateService(ServiceModel service)
        {
            OfferedServices updatedService = _mapper.Map<OfferedServices>(service);
            updatedService.Active = true;
            unitOfWork.Services.Update(updatedService);
            unitOfWork.Save();

            return 0;
        }
    }
}
