﻿using AutoMapper;
using BAL.Interfaces;
using BAL.Models;
using BAL.Utilities;
using DAL.Entities;
using DAL.Interfaces;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAL.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper _mapper;

        public EmployeeService()
        {
            unitOfWork = new UnitOfWork();
            _mapper = CustomAutoMapper.GetInstance().Configuration.CreateMapper();
        }

        public EmployeeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public int AddNewEmployee(EmployeeModel employee)
        {
            //TO DO: EXCEPTION HANDLING
            UserRoles role = unitOfWork.Roles.Get(role => role.RoleName.Equals(employee.RoleName)).FirstOrDefault();

            if (role == null)
            {
                return -1;
            }

            Employees employeeToBeInserted = _mapper.Map<Employees>(employee);
            employeeToBeInserted.Role = role;
            employeeToBeInserted.PasswordHash = Encrypter.HashPassword(employee.Password);

            unitOfWork.Employees.Insert(employeeToBeInserted);

            unitOfWork.Save();
            return 0;
        }

        public int UpdateEmployee(EmployeeModel employee)
        {
            Employees updatedEmployee = _mapper.Map<Employees>(employee);
            Employees oldEmployee = unitOfWork.Employees.GetByID(updatedEmployee.EmployeeId);
            UserRoles role = unitOfWork.Roles.Get(role => role.RoleName.Equals(employee.RoleName)).FirstOrDefault();

            if (oldEmployee == null)
            {
                return -1;
            }

            if (role == null)
            {
                return -2;
            }

            if (employee.Password == null)
            {
                updatedEmployee.PasswordHash = oldEmployee.PasswordHash;
            }
            else
            {
                updatedEmployee.PasswordHash = Encrypter.HashPassword(employee.Password);
            }

            updatedEmployee.Role = role;
            updatedEmployee.DateOfEnrollement = oldEmployee.DateOfEnrollement;

            unitOfWork.Employees.Update(updatedEmployee);
            unitOfWork.Save();
            return 0;
        }

        public int AddUnavailableDay(int employeeId, DateTime day)
        {
            UnavailableDays unavDay = new UnavailableDays
            {
                EmployeeId = employeeId,
                UnavailableDate = day.Date
            };
            unitOfWork.UnavailableDays.Insert(unavDay);
            unitOfWork.Save();
            return 0;
        }

        public EmployeeModel CheckCredentials(string username, string password)
        {
            Employees employee = unitOfWork.Employees.Get(emp => emp.Username.Equals(username)).FirstOrDefault();

            if (employee != null && Encrypter.VerifyHashed(employee?.PasswordHash, password))
            {
                EmployeeModel empl = _mapper.Map<EmployeeModel>(employee);
                return empl;
            }

            return null;
        }

        public int DeleteEmployee(int employeeId)
        {
            Employees employee = unitOfWork.Employees.GetByID(employeeId);
            UserRoles role = unitOfWork.Roles.Get(role => role.RoleName.Equals("INACTIVE")).FirstOrDefault();
            employee.RoleId = role.RoleId;
            unitOfWork.Employees.Update(employee);
            unitOfWork.Save();
            return 0;
        }

        public IEnumerable<EmployeeModel> GetEmployees()
        {
            IEnumerable<Employees> employees = unitOfWork.Employees.Get(employee => !employee.Role.RoleName.Equals("INACTIVE"));

            return employees.Select(emp => _mapper.Map<EmployeeModel>(emp)).ToList();
        }

        public int RemoveUnavailableDay(int employeeId, DateTime unavDay)
        {
            UnavailableDays day = unitOfWork.UnavailableDays.Get(day => day.EmployeeId == employeeId
                                           && DateTime.Compare((DateTime)day.UnavailableDate, unavDay.Date) == 0).FirstOrDefault();
            if (day == null)
            {
                return -1;
            }

            unitOfWork.UnavailableDays.Delete(day.DayId);
            unitOfWork.Save();
            return 0;
        }

        public EmployeeModel GetEmployee(int employeeId)
        {
            return _mapper.Map<EmployeeModel>(unitOfWork.Employees.GetByID(employeeId));
        }

        public IEnumerable<UnavailableDayModel> GetUnavailableDays(int employeeId)
        {
            IEnumerable<UnavailableDays> days = unitOfWork.UnavailableDays.Get(day => day.EmployeeId == employeeId);

            return days.Select(day => _mapper.Map<UnavailableDayModel>(day));
        }
    }
}
