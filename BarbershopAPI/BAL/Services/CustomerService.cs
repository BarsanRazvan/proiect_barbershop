﻿using AutoMapper;
using BAL.Interfaces;
using BAL.Models;
using BAL.Utilities;
using DAL.Entities;
using DAL.Helpers;
using DAL.Interfaces;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAL.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper _mapper;

        public CustomerService()
        {
            unitOfWork = new UnitOfWork();
            _mapper = CustomAutoMapper.GetInstance().Configuration.CreateMapper();
        }
        public int AddBlackList(int customerId, string reason)
        {
            BlackList blackList = new BlackList
            {
                BlackListReason = reason,
                CustomerId = customerId
            };

            unitOfWork.BlackList.Insert(blackList);
            unitOfWork.Save();

            return 0;
        }

        public int AddNewCustomer(CustomerModel customer)
        {
            unitOfWork.Customers.Insert(_mapper.Map<Customers>(customer));
            unitOfWork.Save();
            return 0;
        }

        public CustomerModel GetCustomer(string phoneNumber)
        {
            Customers customer = unitOfWork.Customers.Get(cust => cust.PhoneNumber.Equals(phoneNumber)).FirstOrDefault();
            if(customer == null)
            {
                return null;
            }

            return _mapper.Map<CustomerModel>(customer);
        }

        public PagedModel<CustomerModel> GetCustomers(int pageNumber, int pageSize)
        {
            PagedList<Customers> pagedCustomers = unitOfWork.Customers.GetPagedCustomers(pageNumber, pageSize);
            PagedModel<CustomerModel> pagedModel = new PagedModel<CustomerModel>
            {
                TotalCount = pagedCustomers.TotalCount,
                PageSize = pagedCustomers.PageSize,
                CurrentPage = pagedCustomers.CurrentPage,
                TotalPages = pagedCustomers.TotalPages,
                HasNext = pagedCustomers.HasNext,
                HasPrevious = pagedCustomers.HasPrevious,
                Records = _mapper.Map<IEnumerable<CustomerModel>>(pagedCustomers)
            };

            return pagedModel;
        }

        public int RemoveBlackList(int customerId)
        {
            BlackList blacklist = unitOfWork.BlackList.Get(black => black.CustomerId == customerId
                                                                    && black.Active == true).FirstOrDefault();

            if(blacklist == null)
            {
                return -1;
            }

            blacklist.Active = false;
            unitOfWork.BlackList.Update(blacklist);
            unitOfWork.Save();
            return 0;
        }

        public int UpdateCustomer(CustomerModel customer)
        {
            Customers updatedCustomer = _mapper.Map<Customers>(customer);
            unitOfWork.Customers.Update(updatedCustomer);
            unitOfWork.Save();
            return 0;
        }

        public BlackListModel CheckBlacklist(int customerId)
        {
            BlackList blacklist = unitOfWork.BlackList.Get(black => black.CustomerId == customerId
                                                        && black.Active == true).FirstOrDefault();
            if(blacklist == null)
            {
                return null;
            }

            return _mapper.Map<BlackListModel>(blacklist);    
        }

        public CustomerModel GetCustomer(int customerId)
        {
            Customers customer = unitOfWork.Customers.GetByID(customerId);
            if(customer == null)
            {
                return null;
            }

            return _mapper.Map<CustomerModel>(customer);
        }

    }
}
