﻿using BAL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace BAL.Utilities
{
    class Encrypter
    {
        private Encrypter()
        {

        }

        public static string HashPassword(string password)
        {
            byte[] salt;
            byte[] buffer2;
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            return Convert.ToBase64String(dst);
        }

        public static bool VerifyHashed(string hashedPassword, string password)
        {
            PasswordHasher<EmployeeModel> passwordHasher = new PasswordHasher<EmployeeModel> ();
            return passwordHasher.VerifyHashedPassword(new EmployeeModel(), hashedPassword, password) != PasswordVerificationResult.Failed;
        }
    }
}
