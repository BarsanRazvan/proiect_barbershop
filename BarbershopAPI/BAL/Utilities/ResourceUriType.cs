﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Utilities
{
    public enum ResourceUriType
    {
        PreviousPage,
        NextPage
    }
}
