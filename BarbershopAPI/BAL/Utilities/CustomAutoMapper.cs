﻿using AutoMapper;
using BAL.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAL.Utilities
{
    public class CustomAutoMapper
    {
        private static CustomAutoMapper _instance = null;
        public MapperConfiguration Configuration { get; set; }

        private CustomAutoMapper()
        {
            var profiles = typeof(EmployeeProfile).Assembly.GetTypes().Where(x => typeof(Profile).IsAssignableFrom(x));

            Configuration = new MapperConfiguration(cfg => {
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(Activator.CreateInstance(profile) as Profile);
                }
            });

        }

        public static CustomAutoMapper GetInstance()
        {
            if (_instance == null)
            {
                _instance = new CustomAutoMapper();
            }

            return _instance;
        }
    }
}
