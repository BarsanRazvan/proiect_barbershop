﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Models
{
    public class PagedModel<T>
    { 
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public string PreviousPageLink { get; set; }
        public string NextPageLink { get; set; }
        public bool HasPrevious { get; set; }
        public bool HasNext { get; set; }
        public IEnumerable<T> Records { get; set; }
    }
}
