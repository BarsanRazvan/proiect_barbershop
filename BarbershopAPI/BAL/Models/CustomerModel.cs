﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Models
{
    public class CustomerModel
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FacebookProfile { get; set; }
        public string PhoneNumber { get; set; }
        public int NrOfAppointments { get; set; }
    }
}
