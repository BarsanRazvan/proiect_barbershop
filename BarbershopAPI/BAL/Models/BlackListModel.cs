﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Models
{
    public class BlackListModel
    {
        public DateTime BlackListDate { get; set; }
        public string BlackListReason { get; set; }
    }
}
