﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BAL.Models
{
    public class AppointmentModel
    {
        public int AppointmentId { get; set; }
        public DateTime DateOfAppointment { get; set; }
        public int Duration { get; set; }
        public int StateOfAppointment { get; set; }
        public double TotalPrice { get; set; }
        public int CustomerId { get; set; }
        public int EmployeeId { get; set; }
        public virtual ICollection<AppointmentDetailsModel> AppointmentDetails { get; set; }

        public AppointmentModel()
        {
            AppointmentDetails = new List<AppointmentDetailsModel>();
            TotalPrice = 0;
        }

        public void AddDetails(AppointmentDetailsModel details)
        {
            TotalPrice += details.SellingPrice;
            AppointmentDetails.Add(details);
        }
    }
}
