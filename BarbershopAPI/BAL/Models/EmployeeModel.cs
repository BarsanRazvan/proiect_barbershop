﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace BAL.Models
{
    public class EmployeeModel
    {
        public int EmployeeId { get; internal set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { internal get; set; }
        public int NoOfAppointments { get; set; }
        public String RoleName { get; set; }
    }
}
