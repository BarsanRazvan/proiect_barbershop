﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Models
{
    public class ServiceModel
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public double ServicePrice { get; set; }
        public int ServiceDuration { get; set; }
    }
}
