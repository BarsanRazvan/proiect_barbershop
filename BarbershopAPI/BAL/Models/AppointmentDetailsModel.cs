﻿namespace BAL.Models
{
    public class AppointmentDetailsModel
    {
        public int DetailsId { get; set; }
        public int ServiceId { get; set; }
        public int AppointmentId { get; set; }
        public string ServiceName { get; set; }
        public double ServicePrice { get; set; }
        public double SellingPrice { get; set; }
    }
}
