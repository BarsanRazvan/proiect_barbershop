﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Models
{
    public class UnavailableDayModel
    {
        public int DayId { get; set; }
        public DateTime? UnavailableDate { get; set; }
    }
}
