﻿using AutoMapper;
using BAL.Models;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Profiles
{
    class BlackListProfile : Profile
    {
        public BlackListProfile()
        {
            CreateMap<BlackList, BlackListModel>().ReverseMap();
        }
    }
}
