﻿using AutoMapper;
using BAL.Models;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Profiles
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<Employees, EmployeeModel>()
                .ForMember(dest => dest.RoleName,
                          opt => opt.MapFrom(src => src.Role.RoleName))
                .ReverseMap();
        }
    }
}
