﻿using AutoMapper;
using BAL.Models;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Profiles
{
    public class ServiceProfile : Profile
    {
        public ServiceProfile()
        {
            CreateMap<OfferedServices, ServiceModel>().ReverseMap();
        }
    }
}
