﻿using AutoMapper;
using BAL.Models;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Profiles
{
    public class AppointmentProfile : Profile
    {
        public AppointmentProfile()
        {
            CreateMap<Appointments, AppointmentModel>()
                .ForMember(dest => dest.AppointmentDetails,
                          opt => opt.MapFrom(src => src.AppointmentDetails))
                .ReverseMap();
        }
    }
}
