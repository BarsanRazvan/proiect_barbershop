﻿using AutoMapper;
using BAL.Models;
using DAL.Entities;

namespace BAL.Profiles
{
    public class AppointmentDetailsProfile : Profile
    {
        public AppointmentDetailsProfile()
        {
            CreateMap<AppointmentDetails, AppointmentDetailsModel>().ReverseMap();
        }
    }
}
