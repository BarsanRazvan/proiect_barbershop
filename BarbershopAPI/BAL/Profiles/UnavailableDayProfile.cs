﻿using AutoMapper;
using BAL.Models;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Profiles
{
    public class UnavailableDayProfile : Profile
    {
        public UnavailableDayProfile()
        {
            CreateMap<UnavailableDays, UnavailableDayModel>().ReverseMap();
        }
    }
}
