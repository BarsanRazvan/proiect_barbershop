﻿using DAL.DbContexts;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using System;


namespace DAL.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private DatabaseContext _context;
        private IRepository<Appointments> _appointmentsRepository;
        private IRepository<OfferedServices> _servicesReposistory;
        private IRepository<Employees> _employeesReposistory;
        private ICustomersRepository _customersReposistory;
        private IRepository<UnavailableDays> _unavailableDaysReposistory;
        private IRepository<UserRoles> _rolesReposistory;
        private IRepository<BlackList> _blacklistReposistory;


        private bool disposed = false;

        public UnitOfWork()
        {
            _context = new DatabaseContext();
        }

        public IRepository<Appointments> Appointments
        {
            get
            {

                if (this._appointmentsRepository == null)
                {
                    this._appointmentsRepository = new GenericRepository<Appointments>(_context);
                }
                return _appointmentsRepository;
            }
        }

        public IRepository<OfferedServices> Services
        {
            get
            {
                if (this._servicesReposistory == null)
                {
                    this._servicesReposistory = new GenericRepository<OfferedServices>(_context);
                }
                return _servicesReposistory;
            }
        }

        public IRepository<UnavailableDays> UnavailableDays
        {
            get
            {
                if (this._unavailableDaysReposistory == null)
                {
                    this._unavailableDaysReposistory = new GenericRepository<UnavailableDays>(_context);
                }
                return _unavailableDaysReposistory;
            }
        }

        public IRepository<Employees> Employees
        {
            get
            {
                if (this._employeesReposistory == null)
                {
                    this._employeesReposistory = new GenericRepository<Employees>(_context);
                }
                return _employeesReposistory;
            }
        }

        public ICustomersRepository Customers
        {
            get
            {
                if (this._customersReposistory == null)
                {
                    this._customersReposistory = new CustomersRepository(_context);
                }
                return _customersReposistory;
            }
        }

        public IRepository<UserRoles> Roles
        {
            get
            {
                if (this._rolesReposistory == null)
                {
                    this._rolesReposistory = new GenericRepository<UserRoles>(_context);
                }
                return _rolesReposistory;
            }
        }

        public IRepository<BlackList> BlackList
        {
            get
            {
                if (this._blacklistReposistory == null)
                {
                    this._blacklistReposistory = new GenericRepository<BlackList>(_context);
                }
                return _blacklistReposistory;
            }
        }


        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
