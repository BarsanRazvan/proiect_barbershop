﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public partial class Customers
    {
        public Customers()
        {
            Appointments = new HashSet<Appointments>();
            BlackList = new HashSet<BlackList>();
        }

        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FacebookProfile { get; set; }
        public string PhoneNumber { get; set; }
        public int? NrOfAppointments { get; set; }

        public virtual ICollection<Appointments> Appointments { get; set; }
        public virtual ICollection<BlackList> BlackList { get; set; }
    }
}
