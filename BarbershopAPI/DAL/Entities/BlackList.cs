﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public partial class BlackList
    {
        public int BlackListId { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? BlackListDate { get; set; }
        public string BlackListReason { get; set; }
        public bool? Active { get; set; }
        public virtual Customers Customer { get; set; }
    }
}
