﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public partial class Appointments
    {
        public Appointments()
        {
            AppointmentDetails = new HashSet<AppointmentDetails>();
        }

        public int AppointmentId { get; set; }
        public int? CustomerId { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime DateOfAppointment { get; set; }
        public DateTime? CreationDate { get; set; }
        public int Duration { get; set; }
        public int? StateOfAppointment { get; set; }
        public double TotalPrice { get; set; }

        public virtual Customers Customer { get; set; }
        public virtual Employees Employee { get; set; }
        public virtual ICollection<AppointmentDetails> AppointmentDetails { get; set; }
    }
}
