﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public partial class UnavailableDays
    {
        public int DayId { get; set; }
        public DateTime? UnavailableDate { get; set; }
        public int? EmployeeId { get; set; }

        public virtual Employees Employee { get; set; }
    }
}
