﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public partial class OfferedServices
    {
        public OfferedServices()
        {
            AppointmentDetails = new HashSet<AppointmentDetails>();
        }

        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public double ServicePrice { get; set; }
        public int ServiceDuration { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<AppointmentDetails> AppointmentDetails { get; set; }
    }
}
