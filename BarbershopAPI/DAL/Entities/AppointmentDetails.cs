﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public partial class AppointmentDetails
    {
        public int DetailsId { get; set; }
        public int? ServiceId { get; set; }
        public int? AppointmentId { get; set; }
        public string ServiceName { get; set; }
        public double ServicePrice { get; set; }
        public double SellingPrice { get; set; }

        public virtual Appointments Appointment { get; set; }
        public virtual OfferedServices Service { get; set; }
    }
}
