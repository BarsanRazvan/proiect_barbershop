﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public partial class Employees
    {
        public Employees()
        {
            Appointments = new HashSet<Appointments>();
            UnavailableDays = new HashSet<UnavailableDays>();
        }

        public int EmployeeId { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? RoleId { get; set; }
        public int? NoOfAppointments { get; set; }
        public DateTime? DateOfEnrollement { get; set; }

        public virtual UserRoles Role { get; set; }
        public virtual ICollection<Appointments> Appointments { get; set; }
        public virtual ICollection<UnavailableDays> UnavailableDays { get; set; }
    }
}
