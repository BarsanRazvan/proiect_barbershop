﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public partial class UserRoles
    {
        public UserRoles()
        {
            Employees = new HashSet<Employees>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public virtual ICollection<Employees> Employees { get; set; }
    }
}
