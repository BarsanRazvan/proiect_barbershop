﻿using DAL.Entities;
using DAL.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface ICustomersRepository : IRepository<Customers>
    {
        public PagedList<Customers> GetPagedCustomers(int pageNumber, int pageSize);
    }
}
