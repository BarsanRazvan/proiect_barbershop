﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Appointments> Appointments { get; }
        public IRepository<OfferedServices> Services { get; }
        public IRepository<UnavailableDays> UnavailableDays { get; }
        public IRepository<Employees> Employees { get; }
        public ICustomersRepository Customers { get; }
        public IRepository<UserRoles> Roles { get; }
        public IRepository<BlackList> BlackList { get; }

        void Save();
    }
}
