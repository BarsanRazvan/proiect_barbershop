﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DAL.Entities;
using System.Configuration;

namespace DAL.DbContexts
{
    public partial class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AppointmentDetails> AppointmentDetails { get; set; }
        public virtual DbSet<Appointments> Appointments { get; set; }
        public virtual DbSet<BlackList> BlackList { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<OfferedServices> OfferedServices { get; set; }
        public virtual DbSet<UnavailableDays> UnavailableDays { get; set; }
        public virtual DbSet<UserRoles> UserRoles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(ConfigurationManager.AppSettings["ConnectionString"]);
                optionsBuilder.UseLazyLoadingProxies();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppointmentDetails>(entity =>
            {
                entity.HasKey(e => e.DetailsId)
                    .HasName("PK__Appointm__BAC8628C259ABBBD");

                entity.Property(e => e.ServiceName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Appointment)
                    .WithMany(p => p.AppointmentDetails)
                    .HasForeignKey(d => d.AppointmentId)
                    .HasConstraintName("FK__Appointme__Appoi__52593CB8");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.AppointmentDetails)
                    .HasForeignKey(d => d.ServiceId)
                    .HasConstraintName("FK__Appointme__Servi__5165187F");
            });

            modelBuilder.Entity<Appointments>(entity =>
            {
                entity.HasKey(e => e.AppointmentId)
                    .HasName("PK__Appointm__8ECDFCC2DD272269");

                entity.Property(e => e.CreationDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateOfAppointment).HasColumnType("datetime");

                entity.Property(e => e.StateOfAppointment).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Appointments)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__Appointme__Custo__4AB81AF0");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Appointments)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__Appointme__Emplo__4BAC3F29");
            });

            modelBuilder.Entity<BlackList>(entity =>
            {
                entity.Property(e => e.BlackListDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.BlackListReason)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Active).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.BlackList)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__BlackList__Custo__45F365D3");
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.HasKey(e => e.CustomerId)
                    .HasName("PK__Customer__A4AE64D8B4B35A6D");

                entity.HasIndex(e => e.PhoneNumber)
                    .HasName("UQ__Customer__85FB4E384002AE63")
                    .IsUnique();

                entity.Property(e => e.FacebookProfile)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NrOfAppointments).HasDefaultValueSql("((0))");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.HasKey(e => e.EmployeeId)
                    .HasName("PK__Employee__7AD04F11F6BCB448");

                entity.HasIndex(e => e.Username)
                    .HasName("UQ__Employee__536C85E4F9F0EC2B")
                    .IsUnique();

                entity.Property(e => e.DateOfEnrollement)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NoOfAppointments).HasDefaultValueSql("((0))");

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__Employees__RoleI__3E52440B");
            });

            modelBuilder.Entity<OfferedServices>(entity =>
            {
                entity.HasKey(e => e.ServiceId)
                    .HasName("PK__OfferedS__C51BB00A8FA9654D");

                entity.Property(e => e.Active).HasDefaultValueSql("((1))");

                entity.Property(e => e.ServiceName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UnavailableDays>(entity =>
            {
                entity.HasKey(e => e.DayId)
                    .HasName("PK__Unavaila__BF3DD8C5E2F7C5AB");

                entity.Property(e => e.UnavailableDate).HasColumnType("datetime");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.UnavailableDays)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__Unavailab__Emplo__4E88ABD4");
            });

            modelBuilder.Entity<UserRoles>(entity =>
            {
                entity.HasKey(e => e.RoleId)
                    .HasName("PK__UserRole__8AFACE1AE1A2FE86");

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
