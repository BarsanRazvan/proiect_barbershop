﻿using DAL.DbContexts;
using DAL.Entities;
using DAL.Helpers;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL.Repositories
{
    class CustomersRepository : GenericRepository<Customers>, ICustomersRepository
    {
        public CustomersRepository(DatabaseContext dbContext) : base(dbContext)
        {

        }

        public PagedList<Customers> GetPagedCustomers(int pageNumber, int pageSize)
        {
            var collection = _context.Customers as IQueryable<Customers>;
            return PagedList<Customers>.Create(collection, pageNumber, pageSize);
        }
    }
}
