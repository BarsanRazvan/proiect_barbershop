﻿using BAL.Interfaces;
using BAL.Models;
using BarbershopAPI.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BarbershopAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/accounts/")]
    public class AuthentificationController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly TokenGenerator _tokenGenerator;
        public AuthentificationController(IEmployeeService employeeService, TokenGenerator tokenGenerator)
        {
            _employeeService = employeeService;
            _tokenGenerator = tokenGenerator;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateModel model)
        {
            //TO DO: change to cookie-based JWT
            var user = _employeeService.CheckCredentials(model.Username, model.Password);
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            String token = _tokenGenerator.GenerateToken(user);

            return Ok(new{
                id = user.EmployeeId,
                name = user.FirstName + " " + user.LastName,
                role = user.RoleName,
                token
            });
        }
    }
}
