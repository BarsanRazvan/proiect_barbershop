﻿using BAL.Interfaces;
using BAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BarbershopAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/services/")]
    public class ServicesController : ControllerBase
    {
        private readonly IServicesService _servicesService;
        public ServicesController(IServicesService servicesService)
        {
            _servicesService = servicesService;
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public IActionResult AddNewService([FromBody]ServiceModel model)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_servicesService.AddNewService(model) != 0)
                return BadRequest(new { message = "Invalid model" });

            return Ok(model);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public IActionResult GetAllServices()
        {
            //TO DO: check for sql errors
            IEnumerable<ServiceModel> services = _servicesService.GetServices();

            return Ok(services);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("{serviceId:int}")]
        public IActionResult GetService([FromRoute] int serviceId)
        {
            //TO DO: Extend if with various error cases for easier use of api
            ServiceModel service = _servicesService.GetService(serviceId);
            if (service == null)
                return BadRequest(new { message = "An error has occured" });

            return Ok(service);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPut("{serviceId:int}")]
        public IActionResult UpdateService([FromBody] ServiceModel service)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_servicesService.UpdateService(service) != 0)
                return BadRequest(new { message = "An error has occured" });

            return Ok(new { message = "Service updated successfuly" });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpDelete("{serviceId:int}")]
        public IActionResult RemoveService([FromRoute] int serviceId)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_servicesService.RemoveService(serviceId) != 0)
                return BadRequest(new { message = "An error has occured" });

            return Ok(new { message = "Service deleted successfuly" });
        }
    }
}
