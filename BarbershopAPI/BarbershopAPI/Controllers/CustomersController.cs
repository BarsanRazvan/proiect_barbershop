﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using BAL.Interfaces;
using BAL.Models;
using BAL.Utilities;
using BarbershopAPI.ResourceParameters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BarbershopAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/customers/")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        public CustomersController(ICustomerService customerSerivce)
        {
            _customerService = customerSerivce;
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public IActionResult AddNewCustomer([FromBody]CustomerModel model)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_customerService.AddNewCustomer(model) != 0)
                return BadRequest(new { message = "Invalid model" });

            return Ok(model);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet(Name = "GetCustomers")]
        public IActionResult GetCustomers([FromQuery] CustomersResourceParameters customersResourceParameters)
        {
            PagedModel<CustomerModel> customers =
                _customerService.GetCustomers(customersResourceParameters.PageNumber, customersResourceParameters.PageSize);

            customers.PreviousPageLink = customers.HasPrevious ? 
                CreateAuthorsResourceUri(customersResourceParameters,ResourceUriType.PreviousPage) 
                : null;

            customers.NextPageLink = customers.HasNext ?
                CreateAuthorsResourceUri(customersResourceParameters, ResourceUriType.NextPage) 
                : null;

            var paginationMetadata = new
            {
                totalCount = customers.TotalCount,
                pageSize = customers.PageSize,
                currentPage = customers.CurrentPage,
                totalPages = customers.TotalPages,
                previousPageLink = customers.PreviousPageLink,
                nextPageLink = customers.NextPageLink
            };

            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(paginationMetadata));

            return Ok(customers.Records);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("phoneNumber={phoneNumber}")]
        public IActionResult GetCustomer(string phoneNumber)
        {
            CustomerModel customer = _customerService.GetCustomer(phoneNumber);
            if (customer == null)
                return BadRequest(new { message = "An error has occured" });

            return Ok(customer);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPut("{customerId:int}")]
        public IActionResult UpdateCustomer([FromBody] CustomerModel customer)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_customerService.UpdateCustomer(customer) != 0)
                return BadRequest(new { message = "An error has occured" });

            return Ok(new { message = "User successfully updated" });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("{customerId:int}")]
        public IActionResult GetCustomer([FromRoute] int customerId)
        {
            //TO DO: Extend if with various error cases for easier use of api
            CustomerModel customer = _customerService.GetCustomer(customerId);
            if (customer == null)
                return BadRequest(new { message = "An error has occured" });

            return Ok(customer);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost("{customerId:int}/blacklist")]
        public IActionResult BlackListCustomer([FromRoute] int customerId, [FromBody] string reason)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_customerService.AddBlackList(customerId, reason) != 0)
                return BadRequest(new { message = "An error has occured" });

            return Ok(new { message = "User successfully blacklisted" });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpDelete("{customerId:int}/blacklist")]
        public IActionResult RemoveBlackList([FromRoute] int customerId)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_customerService.RemoveBlackList(customerId) != 0)
                return BadRequest(new { message = "An error has occured" });

            return Ok(new { message = "Blacklist successfully removed" });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("{customerId:int}/blacklist")]
        public IActionResult GetBlackList([FromRoute] int customerId)
        {
            //TO DO: Extend if with various error cases for easier use of api
            BlackListModel blacklist = _customerService.CheckBlacklist(customerId);
            if (blacklist != null)
                return BadRequest(new { message = "An error has occured" });

            return Ok(blacklist);
        }

        private string CreateAuthorsResourceUri(CustomersResourceParameters customersResourceParameters, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return Url.Link("GetCustomers",
                      new
                      {
                          pageNumber = customersResourceParameters.PageNumber - 1,
                          pageSize = customersResourceParameters.PageSize,
                      });
                case ResourceUriType.NextPage:
                    return Url.Link("GetCustomers",
                      new
                      {
                          pageNumber = customersResourceParameters.PageNumber + 1,
                          pageSize = customersResourceParameters.PageSize,
                      });

                default:
                    return Url.Link("GetCustomers",
                    new
                    {
                        pageNumber = customersResourceParameters.PageNumber,
                        pageSize = customersResourceParameters.PageSize,
                    });
            }

        }
    }
}