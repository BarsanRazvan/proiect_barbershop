﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BAL.Interfaces;
using BAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BarbershopAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/appointments")]
    public class AppointmentsController : Controller
    {
        private readonly IAppointmentService _appointmentService;
        public AppointmentsController(IAppointmentService appointmentService)
        {
            _appointmentService = appointmentService;
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("employeeId={employeeId}&date={date}&finished={finished}")]
        public IActionResult GetAppointmentByDate(int employeeId, DateTime date, Boolean finished)
        {
            IEnumerable<AppointmentModel> appointments = _appointmentService.GetAppointmentsByDate(date, employeeId, finished);
            return Ok(appointments);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("employeeId={employeeId}&dateTime={dateTime}")]
        public IActionResult GetAppointmentByDateTime(int employeeId, DateTime dateTime)
        {
            AppointmentModel appointment = _appointmentService.GetAppointmentByDateTime(dateTime, employeeId);
            if (appointment == null)
            {
                return BadRequest(new { message = "No appointment at given date" });
            }
            return Ok(appointment);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPut]
        public IActionResult MakeNewAppointment([FromBody] AppointmentModel appointment)
        {
            if (_appointmentService.MakeNewAppointment(appointment) != 0)
            {
                return BadRequest(new { message = "No appointment at given date" });
            }

            return Ok(new { message = "Successfully added new appointment" });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost("{appointmentId}")]
        public IActionResult FinishAppointment(int appointmentId)
        {
            if (_appointmentService.FinishAppointment(appointmentId) != 0)
            {
                return BadRequest(new { message = "No appointment at given date" });
            }

            return Ok(new { message = "Successfully finished appointment" });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpDelete("{appointmentId}")]
        public IActionResult CancelAppointment(int appointmentId)
        {
            if (_appointmentService.CancelAppointment(appointmentId) != 0)
            {
                return BadRequest(new { message = "No appointment at given date" });
            }

            return Ok(new { message = "Successfully canceled appointment" });
        }

    }
}
