﻿using BAL.Interfaces;
using BAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BarbershopAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/employees/")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        public EmployeesController(IEmployeeService employeeSerivce)
        {
            _employeeService = employeeSerivce;
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public IActionResult AddNewEmployee([FromBody]EmployeeModel model)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_employeeService.AddNewEmployee(model) != 0)
                return BadRequest(new { message = "Invalid model" });

            return Ok(model);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public IActionResult GetAllEmployees()
        {
            //TO DO: check for sql errors
            IEnumerable<EmployeeModel> employees = _employeeService.GetEmployees();
            return Ok(employees);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpDelete("{employeeId:int}")]
        public IActionResult DeleteEmployee(int employeeId)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_employeeService.DeleteEmployee(employeeId) != 0)
                return BadRequest(new { message = "An error has occured" });

            return Ok(new { message = "User successfully deleted" });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("{employeeId:int}")]
        public IActionResult GetEmployee(int employeeId)
        {
            //TO DO: Extend if with various error cases for easier use of api
            EmployeeModel employee = _employeeService.GetEmployee(employeeId);
            if (employee == null)
                return BadRequest(new { message = "The required employee does not exists" });

            return Ok(employee);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("{employeeId:int}/unavailabledays")]
        public IActionResult GetUnavailableDays(int employeeId)
        {
            //TO DO: Extend if with various error cases for easier use of api
            IEnumerable<UnavailableDayModel> days = _employeeService.GetUnavailableDays(employeeId);

            return Ok(days);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost("{employeeId:int}/unavailabledays")]
        public IActionResult AddUnavailableDay([FromRoute]int employeeId, [FromBody] DateTime day)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_employeeService.AddUnavailableDay(employeeId, day) != 0)
            {
                return BadRequest(new {message = "Invalid employee or day"});
            }

            return Ok(new { message = "Date added successfully" });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpDelete("{employeeId:int}/unavailabledays")]
        public IActionResult RemoveUnavailableDay([FromRoute]int employeeId, [FromBody] DateTime day)
        {
            //TO DO: Extend if with various error cases for easier use of api
            if (_employeeService.RemoveUnavailableDay(employeeId, day) != 0)
            {
                return BadRequest(new { message = "Invalid employee or day" });
            }

            return Ok(new { message = "Date deleted successfully" });
        }
    }
}
